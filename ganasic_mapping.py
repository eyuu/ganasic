#!/usr/bin/env python3

import sys
import os
import argparse
import numpy as np
import json
from core import ganasic_utils


def main():

    version = '0.1.2'

    usage = """ganasic_mapping.py -i file.in -o file.out [options]

    Input:
    (-i) text file with the path of the fasta reference sequences, one per line
    (-f) fastq file to classify

    Output:
    (-o) output prefix for files _mapped_counts.npy and _total.npy

    See the provided LICENSE file or source code for license information.
    """
    
    parser = argparse.ArgumentParser(usage=usage)

    parser.add_argument('-i', '--input-file', type=str, dest='input_file', required=True, help='Input file with path to the fasta reference sequences (one per line)')
    parser.add_argument('-f', '--fastq-file', type=str, dest='fastq_file', required=True, help='Fastq file to classify')
    parser.add_argument('-o', '--output-prefix', type=str, dest='output_prefix', required=True, help='Output prefix for results')
    
    parser.add_argument('-m','--mapper', type=str, choices=["ganon", "kallisto"], dest='mapper', default="ganon", help='Mapping method to use (ganon, kallisto). Default: ganon')
    parser.add_argument('-c','--mapper-classify-args', type=str, dest='mapper_classify_args', default="", help='Arguments for the classification step of the mapper')
    
    parser.add_argument('-t', '--threads', type=int, default=1, dest='threads', help='Number of threads to use. Default: 1')
    parser.add_argument('--temp', type=str, dest='temp', default='./temp', help='Directory to store temporary files. Default: ./temp')
    parser.add_argument('-v', '--version', action='version', version='version: %(prog)s ' + version, help="Show program's version number and exit.")
    
    args = parser.parse_args()

    if len(sys.argv[1:])==0:
        parser.print_help() 
        sys.exit(1)

    threads = args.threads
    output_path = args.temp + "/"
    if not os.path.exists(output_path):
        print("Temp directory must exist with indices")
        sys.exit(1)

    fasta_files = [ line.strip() for line in open(args.input_file, "r").readlines() ]
    def get_file_id(file): return os.path.basename(file)
    n_files = len(fasta_files)
    sim_files = [ output_path + get_file_id(file) + '.fastq' for file in fasta_files ]
    db_prefix = output_path + args.mapper + "_index"  
    fastq_name = os.path.basename(args.fastq_file)

    # Map reads
    print("Mapping reads with " + args.mapper)
    extra_params = " -t " + str(args.threads) 
    if args.mapper=="kallisto":
        if not args.mapper_classify_args: 
            extra_params+= " -l 100 -s 20 "
            print("WARNING: -l 100 -s 20 is used as default for kallisto, to change please provide a different value with -c")

    ganasic_utils.run_mapper_classify[args.mapper](output_path, 
                                    "mapping_" + args.mapper + "_" + fastq_name, 
                                    args.fastq_file, 
                                    db_prefix,
                                    args.mapper_classify_args + extra_params )


    print("Counting mapped reads")
    mapped_array = [] 
    if args.mapper=="ganon":
        # read assembly filename aux file
        assembly_name = {}
        with open(db_prefix + ".assembly", "r") as file:
            for line in file:
                if line:
                    assembly, name = line.rstrip().split("\t")
                    assembly_name[assembly] = name
        
        ordered_names = [get_file_id(file) for file in fasta_files]

        classification_output = output_path + "mapping_" + args.mapper + "_" + fastq_name + ".rep"
        mapped_array = ganasic_utils.ganon_counts(classification_output, 
                    assembly_name,
                    ordered_names )

        # get total counts
        with open(output_path + "mapping_" + args.mapper + "_" + fastq_name + ".log") as log_file:
            for line in log_file:
                if line.startswith("ganon-classify processed"):
                    total = int(line.split(" ")[2])
                    break

    elif args.mapper=="kallisto":
        # Read aux contig counts
        contig_index = []
        with open(db_prefix + ".contigs", "r") as file:
            for line in file:
                if line.rstrip(): contig_index.append(int(line.rstrip()))

        classification_output = output_path + "mapping_" + args.mapper + "_" + fastq_name
        mapped_array = ganasic_utils.kallisto_counts(classification_output, contig_index)

        # get total counts
        with open(classification_output + "/run_info.json") as json_data:
            data = json.load(json_data)
            total = int(data["n_processed"])               

    np.save(args.output_prefix+"_mapped_counts.npy", mapped_array)
    np.save(args.output_prefix+"_total.npy", np.array([total]).astype(np.float64))
    print("Mapping counts and total generated and saved to: " + args.output_prefix+"_mapped_counts.npy, " + args.output_prefix+"_total.npy")
    
if __name__=="__main__":
    main()

   
