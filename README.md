# ganasic (DiTASiC+ganon)

Based on DiTASiC code, but now supporting kallisto and ganon. Simulate reads with dwgsim or mason.

To install the required tools please use conda:

```ssh
git clone https://gitlab.com/dacs-hpi/ganasic.git
conda env create -f ganasic_env.yaml
conda activate ganasic_env
```

## Running with sample data

### unpack sample data

```ssh
cd ganasic
tar xf sample_data.tar.gz
```

### Generate similarity matrix for 34 reference files with ganon and dwgsim

```ssh
./ganasic_matrix.py -i sample_data/reference_paths -o matrix_ganon -s dwgsim -m ganon -l 100 -n 10000 -t 8
```

### Map the samples to the reference files for the raw abundance values.

```ssh
./ganasic_mapping.py -i sample_data/reference_paths -f sample_data/sample1.fastq -o mapping_ganon_sample1 -m ganon -t 8

./ganasic_mapping.py -i sample_data/reference_paths -f sample_data/sample2.fastq -o mapping_ganon_sample2 -m ganon -t 8
```

### Estimate abundance for sample 1

```ssh
./ditasic -r sample_data/reference_paths -a matrix_ganon.npy -x mapping_ganon_sample1_mapped_counts.npy -n mapping_ganon_sample1_total.npy -f F -t 750 -o ganon_sample1_abundance.txt
```

### Estimate abundance for sample 2

```ssh
./ditasic -r sample_data/reference_paths -a matrix_ganon.npy -x mapping_ganon_sample2_mapped_counts.npy -n mapping_ganon_sample2_total.npy -f F -t 750 -o ganon_sample2_abundance.txt
```

### Estimate differential abundance between sample 1 and sample 2

```ssh
./ditasic -r sample_data/reference_paths -a matrix_ganon.npy \
          -x mapping_ganon_sample1_mapped_counts.npy -n mapping_ganon_sample1_total.npy \
          -y mapping_ganon_sample2_mapped_counts.npy -m mapping_ganon_sample2_total.npy \
          -f F -t 750 -o ganon_sample1_sample2_diff_abundance.txt
```
