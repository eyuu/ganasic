#!/usr/bin/env python3

import os
import sys
import subprocess
import shlex
import shutil
import gzip
import pickle
import numpy as np

def run_kallisto_index(db_prefix, input_files, args):
    command   = "kallisto index -i {index} {args} {files}".format(
        index=db_prefix,
        files=' '.join("'{0}'".format(f) for f in input_files),
        args=args)
    errcode, _, _ = run(command)
    if errcode!=0: return False

    # count number of contigs in the references
    contigs = []
    for i,ref in enumerate(input_files):
        fai_path = ref+".fai"
        if (os.path.exists(fai_path)):
            with open(fai_path,"r") as fai_file:
                contigs.extend([i]*len(fai_file.readlines()))
        else:
            with open(ref) as f:
                for line in f:
                    if line.startswith(">"):
                        contigs.append(i)

    print("\n".join(str(ctg) for ctg in contigs), file=open(db_prefix+".contigs", "w"))
    return True

def run_kallisto_pseudo(output_path, fasta_name , reads_file, db_prefix, args):
    command = "kallisto pseudo -i {index} -o '{samfile}' {args} --single '{reads}'".format(
                reads=reads_file, 
                index=db_prefix, 
                samfile=output_path + fasta_name, 
                args=args)
    errcode, _, _ = run(command)
    return output_path + fasta_name if errcode==0 else False

def kallisto_counts(tsv_dir, contig_index):

    ref_size = contig_index[-1]+1
    uniq_n = len(contig_index)

    vec_unique = [0.]*ref_size
    vec_shared = [0.]*ref_size

    # pseudoalignments mapped to sets of contigs
    with open(tsv_dir + "/pseudoalignments.tsv") as tsv:
        pseudo = [ int(line.split()[1]) for line in tsv.readlines() ]

    # equivalence classes
    ec_index = []
    with open(tsv_dir + "/pseudoalignments.ec") as ec:
        ec_index = [ (line.split()[1]).split(",") for line in ec.readlines() ]

    # sum the equivalence classes as they distribute reads among the reference contigs
    for i, ec in enumerate(ec_index):
        # the contig received this many hits
        contig_count = pseudo[i]

        # for each of the contigs covered by the EC got the hits
        processed_refs = set()
        added = []
        for contig in ec:
            # the hits go to this reference
            ref = contig_index[int(contig)]

            # only add to each reference once
            if ref in processed_refs:
                continue

            processed_refs.add(ref)
            added.append( (ref, contig_count) )

            if i < uniq_n:
                vec_unique[ref] += contig_count
            else:
                vec_shared[ref] += contig_count

        assert( len(processed_refs) == len(added) )

    return np.array(vec_unique) + np.array(vec_shared)

def merge_input_files(input_files,db_prefix):
    merged_file = db_prefix+".merged.fna"
    if os.path.exists(merged_file): os.remove(merged_file)

    with open(merged_file,'wb') as wfd:
        for f in input_files:
            if f.endswith("gz"):
                with gzip.open(f, 'rb') as fd:
                    shutil.copyfileobj(fd, wfd)
            else:
                with open(f,'rb') as fd:
                    shutil.copyfileobj(fd, wfd)

    return merged_file

def run_ganon_build(db_prefix, input_files, args):
    merged_file = ""
    if len(input_files)>10000: merged_file = merge_input_files(input_files, db_prefix) 
    command = "ganon build --verbose --db-prefix {db_prefix} --input-files {input_files} --rank assembly {args}".format(
            db_prefix = db_prefix, 
            input_files = " ".join(input_files) if not merged_file else merged_file,
            args=args)
    errcode, _, stderr = run(command)
    print(stderr,file=open(db_prefix + ".log", "w"))
    if errcode: return False
    if merged_file and os.path.exists(merged_file): os.remove(merged_file)

    acc_name = dict()
    for ref in input_files:
        f = gzip.open(ref, "rt") if ref.endswith("gz") else open(ref)
        for line in f:
            if line.startswith(">"):
                acc = line.split(" ")[0][1:]
                if acc in acc_name: print("WARNING: repeated accession " + acc)
                acc_name[acc] = ref
        f.close()

    assembly_acc = dict()
    gnn = pickle.load(gzip.open(db_prefix + ".gnn", "rb"))
    for line in gnn["bins"]:
        if line:
            field = line.split("\t")
            acc = field[0].split("/")[0]
            assembly = field[3]
            assembly_acc[assembly] = acc
            
    assembly_file = open(db_prefix + ".assembly", "w")
    for assembly, acc in assembly_acc.items():
        print(assembly, os.path.basename(acc_name[acc]), sep="\t", file=assembly_file)
    assembly_file.close()

    return True

def run_ganon_classify(output_path, fasta_name, reads_file, db_prefix, args):
    command = "ganon classify --verbose {args} --db-prefix {db_prefix} --single-reads {single_reads} --output-prefix {output_prefix} --rank all".format(
            db_prefix=db_prefix, 
            single_reads=reads_file,
            output_prefix=output_path + fasta_name,
            args=args)
    errcode, _, stderr = run(command)
    print(stderr,file=open(output_path + fasta_name + ".log", "w"))
    os.remove(output_path + fasta_name + ".lca")
    return output_path + fasta_name + ".rep" if errcode==0 else False

def ganon_counts(rep_file, assembly_name, ordered_names):
    counts = np.zeros(len(ordered_names))
    # finds the name of the file the assembly accession originated and sum counts
    with open(rep_file, "r") as rep:
        for line in rep:
            if line and not line.startswith("#"):
                field = line.rstrip().split("\t")
                if field[5]=="assembly":
                    assembly = field[1]
                    # Skip if match is not in ganon assembly entries
                    if assembly not in assembly_name: 
                        continue
                    # Skip if match file is not on the input entries
                    if assembly_name[assembly] not in ordered_names:
                        continue
                    count = int(field[2])
                    counts[ordered_names.index(assembly_name[assembly])]+=count
                    #print(assembly, count, assembly_name[assembly], ordered_names.index(assembly_name[assembly]))

    return counts

def run_mason(fasta_file, sim_file, read_length, read_number, args):
    command = "mason_simulator -n {num} --illumina-read-length {length} {args} -ir '{ref}' -o '{out}'".format(
                num=read_number, 
                length=read_length, 
                ref=fasta_file, 
                out=sim_file,
                args=args)
    errcode, _, _ = run(command)
    #command = "rm %s.sam"%out
    return sim_file if errcode==0 else False

def run_dwgsim(fasta_file, sim_file, read_length, read_number, args):
    command = "dwgsim -c 0 -1 {length} -2 0 -N {num} {args} {ref} {out}".format(
                length=read_length, 
                num=read_number, 
                ref=fasta_file, 
                out=sim_file,
                args=args)
    errcode, _, _ = run(command)
    #command = "rm %s.sam"%out
    shutil.move(sim_file+".bfast.fastq", sim_file)
    os.remove(sim_file+".bwa.read1.fastq")
    os.remove(sim_file+".bwa.read2.fastq")
    os.remove(sim_file+".mutations.txt")
    os.remove(sim_file+".mutations.vcf")
    return sim_file if errcode==0 else False

def run(cmd):
    errcode=0
    stdout=""
    stderr=""
    try:
        process = subprocess.Popen(shlex.split(cmd), 
            universal_newlines=True, 
            stdout=subprocess.PIPE, 
            stderr=subprocess.PIPE)
        stdout, stderr = process.communicate() # wait for the process to terminate
        errcode = process.returncode
        if errcode: raise Exception()
    except Exception as e:
        print('Command failed to run:\n'+cmd)
        print(str(e))
        print("Error code: "+str(errcode))
        if stdout: 
            print("STDOUT: ")
            print(stdout)
        if stderr: 
            print("STDERR: ")
            print(stderr)
        if not errcode: errcode=1
    return errcode, stdout, stderr 

run_simulator = dict( mason=run_mason,
                      dwgsim=run_dwgsim,)

run_mapper_build = dict(ganon=run_ganon_build,
                        kallisto=run_kallisto_index,)

run_mapper_classify = dict(ganon=run_ganon_classify,
                    kallisto=run_kallisto_pseudo, )
